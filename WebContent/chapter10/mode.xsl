<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:preserve-space elements="text"/>

  <xsl:template match="/">

    <xsl:element name="div">
      <xsl:apply-templates select="//library" />
      <xsl:apply-templates select="//library" mode="mutt" />
    </xsl:element>

  </xsl:template>

  <xsl:template match="library">

    <xsl:element name="table">
      <xsl:attribute name="width">100%</xsl:attribute>

      <xsl:for-each select="book">
        <xsl:element name="tr">

	     <xsl:for-each select="*">
	       <xsl:element name="td">
	         <xsl:attribute name="width">33%</xsl:attribute>

	         <xsl:value-of select="." />

            <xsl:if test="string-length(.) = 0">
	           <xsl:text> </xsl:text>
	         </xsl:if>
	       </xsl:element>
	     </xsl:for-each>
	   </xsl:element>
	 </xsl:for-each>
    </xsl:element>

  </xsl:template>

  <xsl:template match="library" mode="mutt">

    <xsl:element name="table">
      <xsl:attribute name="width">100%</xsl:attribute>

      <xsl:for-each select="book">
        <tr>
          <xsl:for-each select="*">
            <td width="33%">
              <xsl:value-of select="." />

              <xsl:if test="string-length(.) = 0">
	             <xsl:text> </xsl:text>
	           </xsl:if>
	         </td>
	       </xsl:for-each>
        </tr>
      </xsl:for-each>
    </xsl:element>

	</xsl:template>

</xsl:stylesheet>