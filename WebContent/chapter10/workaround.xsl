<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">
    <xsl:variable name="value" select="7" />

    <xsl:element name="div">
      <xsl:choose>
        <xsl:when test="($value mod 2) = 0">Even</xsl:when>
        <xsl:otherwise>Not even</xsl:otherwise>
      </xsl:choose>
    </xsl:element>
  </xsl:template>

</xsl:stylesheet>