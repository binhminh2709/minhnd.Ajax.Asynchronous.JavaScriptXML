<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">
    <xsl:element name="table">
      <xsl:attribute name="width">100%</xsl:attribute>

      <xsl:apply-templates select="//book">
        <xsl:sort select="title" order="descending"  />
	   </xsl:apply-templates>
	 </xsl:element>

  </xsl:template>

  <xsl:template match="*">

    <xsl:element name="tr">
      <xsl:for-each select="*">
        <xsl:element name="td">
          <xsl:value-of select="." />
        </xsl:element>
         </xsl:for-each>
       </xsl:element>

  </xsl:template>

</xsl:stylesheet>