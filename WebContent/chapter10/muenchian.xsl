<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes" />
  <xsl:key name="keyBook" match="book" use="series" />

  <xsl:template match="/">

    <xsl:element name="table">
       <xsl:attribute name="width">100%</xsl:attribute>

       <xsl:apply-templates select="//book[1]" mode="header" />
    <xsl:apply-templates select="//book[generate-id(.) = generate-id(key('keyBook',series)[1])]" />
       <xsl:apply-templates select="//book[string-length(series) = 0]/series" />
  </xsl:element>

   </xsl:template>

<xsl:template match="book">

     <xsl:variable name="key">
       <xsl:value-of select="series" />
     </xsl:variable>

     <xsl:apply-templates select="//series[node() = $key]" />

   </xsl:template>

   <xsl:template match="series">

     <xsl:element name="tr">
       <xsl:apply-templates select="parent::node()/*" mode="cell" />
     </xsl:element>

   </xsl:template>

   <xsl:template match="*" mode="cell">

     <xsl:element name="td">
       <xsl:attribute name="align">left</xsl:attribute>

    <xsl:value-of select="." />
  </xsl:element>

   </xsl:template>

   <xsl:template match="book" mode="header">

  <xsl:element name="tr">
    <xsl:apply-templates select="./*" mode="columnHeader" />
  </xsl:element>

   </xsl:template>

   <xsl:template match="*" mode="columnHeader">

     <xsl:variable name="lowerCase">qwertyuiopasdfghjklzxcvbnm</xsl:variable>
     <xsl:variable name="upperCase">QWERTYUIOPASDFGHJKLZXCVBNM</xsl:variable>

     <xsl:element name="th">
       <xsl:attribute name="width">33%</xsl:attribute>

       <xsl:value-of select="translate(name(.),$lowerCase,$upperCase)" />
     </xsl:element>

   </xsl:template>

</xsl:stylesheet>