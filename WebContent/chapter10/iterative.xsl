<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" version="1.0" encoding="UTF-8"/>

	<xsl:template match="/">
		<xsl:element name="table">

			<xsl:for-each select="//book">
				<xsl:element name="tr">

					<xsl:for-each select="child::*">
						<xsl:element name="td">
							<xsl:value-of select="."/>
						</xsl:element>
					</xsl:for-each>

				</xsl:element>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>