<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" version="1.0" encoding="UTF-8"/>

	<xsl:template match="/">

		<xsl:element name="table">
			<xsl:apply-templates select="//book"/>
		</xsl:element>

	</xsl:template>

	<xsl:template match="*">

		<xsl:if test="count(ancestor::*) = 1">
			<xsl:element name="tr">
				<xsl:apply-templates select="child::*"/>
			</xsl:element>
		</xsl:if>
		<xsl:if test="count(ancestor::*) != 1">
			<xsl:element name="td">
				<xsl:value-of select="."/>
			</xsl:element>
		</xsl:if>

	</xsl:template>

</xsl:stylesheet>