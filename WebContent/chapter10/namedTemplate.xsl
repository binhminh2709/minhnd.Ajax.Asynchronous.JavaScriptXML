<xsl:template name="add">
	<xsl:param name="a" />
	<xsl:param name="b" />

	<xsl:value-of select="number($a) + number($b)" />
</xsl:template>