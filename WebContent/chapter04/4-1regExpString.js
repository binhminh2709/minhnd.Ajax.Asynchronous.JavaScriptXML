function initialize() {
  var colors = 'redorangebluegreenblueyellow';
  /*
   * Call the substitute function twice, once for blue and once for red.
   */
  alert(substitute(substitute(colors, 'blue', 'purple'), 'red', 'purple'));
  /*
   * Define the regular expression to search for red or blue, in addition set the options for global and ignore case.
   * The available options are: g = global (all occurrences in a string) i = ignore case gi = global and ignore case
   */
  var re = new RegExp('red|blue', 'gi');
  /*
   * Perform the replacement.
   */
  alert(colors.replace(re, 'purple'));
}

function substitute(text, word, replacement) {
  var temp = text;
  /*
   * perform string replacement using substring.
   */
  while (temp.indexOf(word) >= 0) {
    temp = temp.substr(0, temp.indexOf(word)) + replacement + temp.substr(temp.indexOf(word) + word.length);
  }
  return (temp);
}