﻿DELIMITER $$

DROP PROCEDURE IF EXISTS `ajax`.`itemSelectXML`$$
CREATE PROCEDURE `ajax`.`itemSelectXML`(
/*
  Procedure:   itemSelectXML
  Programmer:  Edmond Woychowsky
  Purpose      To retrieve all items, item based upon guild or a single item
               based upon the guil-item bridge id.
*/
guildItemId INTEGER,                    /*  Guild-item bridge id               */
guildId INTEGER                         /*  Guild id                           */
)
BEGIN
  DECLARE fini BOOLEAN DEFAULT FALSE;   /*  Processing complete indicator      */
  DECLARE xml LONGTEXT DEFAULT '';      /*  XML text string                    */
  DECLARE cGuildItemId INTEGER(6);
  DECLARE cGuildId INTEGER(6);
  DECLARE cGuildName VARCHAR(255);
  DECLARE cItemName VARCHAR(255);
  DECLARE cItemDescription VARCHAR(255);
  DECLARE cItemPrice DECIMAL(10,2);
  DECLARE itemCursor CURSOR FOR SELECT      b.guild_item_id,
                                            b.guild_id,
                                            g.guild_name,
                                            i.item_name,
                                            i.item_description,
                                            i.item_price
                                FROM        guild_item_bridge b
                                INNER JOIN  guild g
                                ON          b.guild_id = g.guild_id
                                INNER JOIN  item i
                                ON          b.item_id = i.item_id
                                WHERE       (guildItemId IS NULL OR guildItemId = b.guild_item_id)
                                AND         (guildId IS NULL OR guildId = b.guild_id);
  DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET fini = TRUE;

  OPEN itemCursor;

  FETCH itemCursor INTO cGuildItemId,
                        cGuildId,
                        cGuildName,
                        cItemName,
                        cItemDescription,
                        cItemPrice;

  REPEAT                                /*  Process each row                   */
    IF NOT fini THEN
    SET xml = CONCAT(xml,
                    f_xmlNode('item',
                                CONCAT(f_xmlNode('guild_item_id',CAST(cGuildItemId AS CHAR),FALSE),
                                       f_xmlNode('guild_id',CAST(cGuildId AS CHAR),FALSE),
                                       f_xmlNode('guild_name',cGuildName,FALSE),
                                       f_xmlNode('item_name',cItemName,TRUE),
                                       f_xmlNode('item_description',cItemDescription,TRUE),
                                       f_xmlNode('line_item_price',CAST(cItemPrice AS CHAR),FALSE)),
                                FALSE));
    END IF;


  FETCH itemCursor INTO cGuildItemId,
                        cGuildId,
                        cGuildName,
                        cItemName,
                        cItemDescription,
                        cItemPrice;
  UNTIL fini END REPEAT;

  SELECT f_xmlNode('items',xml,FALSE);

  CLOSE itemCursor;
END$$

DELIMITER ;