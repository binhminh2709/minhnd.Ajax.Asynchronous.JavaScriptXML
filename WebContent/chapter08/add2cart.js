/*
   Function:   add2Cart
   Programmer: Edmond Woychowsky
   Purpose:    To add an item/quantity pair to an XML Data Island that represents a
               shopping cart.
*/
function add2Cart() {
  var item = xml.selectSingleNode('//guild_item_id').serialize().replace(new RegExp('<[^<]{0,}>','g'),'');
  var quantity = document.getElementById('quantity').value;
  var re = new RegExp('<item><id>' + item + '</id><quantity>[^<]{1,}</quantity></item>','g');

  if(re.test(document.getElementById('cart').innerHTML))
    document.getElementById('cart').innerHTML = document.getElementById('cart').innerHTML.replace(re,'');

  document.getElementById('cart').innerHTML += '<item><id>' + item + '</id><quantity>' + quantity + '</quantity></item>';

  alert('Item added to cart.');
}
