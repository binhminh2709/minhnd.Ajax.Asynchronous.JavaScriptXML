//<!-- <![CDATA[
SOAPEnvelope.prototype = new SOAPEnvelope;
SOAPEnvelope.prototype.constructor = SOAPEnvelope;

/*
 * Class:         SOAPEnvelope
 * Function:      SOAPEnvelope
 * Method:        n/a
 * Description:   Constructor for this class.
 */
// <!-- <![CDATA[
function SOAPEnvelope() {
  this._template = '<?xml version="1.0" encoding="utf-8"?>';

  this._template += '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">';
  this._template += '<soap:Body>';
  this._template += '<_operator xmlns="_namespace">';
  this._template += '_package';
  this._template += '</_operator>';
  this._template += '</soap:Body>';
  this._template += '</soap:Envelope>';
}

SOAPEnvelope.prototype.operator = null;
SOAPEnvelope.prototype.namespace = 'http://tempuri.org/';
SOAPEnvelope.prototype.content = null;

/*
	Class:			SOAPEnvelope
	Function:		SOAPEnvelope_envelope
	Method:			envelope
	Description:	Returns the readyState for the XMLHTTP Request object.
*/
function SOAPEnvelope_envelope() {
  var work;

  work = this._template.replace(/_operator/g,this.operator);
  work = work.replace(/_namespace/g,this.namespace);
  work = work.replace(/_package/g,this.content);
alert(work);
  return(work);
}
SOAPEnvelope.prototype.envelope = SOAPEnvelope_envelope;
// ]]> -->
