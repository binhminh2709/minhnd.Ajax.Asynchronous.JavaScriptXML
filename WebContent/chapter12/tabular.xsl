<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" indent="yes" media-type="text/html"/>

	<xsl:template match="/">
		<xsl:element name="table">
			<xsl:apply-templates select="/*/*[1]" mode="header" />
			<xsl:apply-templates select="/*/*" mode="row" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="*" mode="header">
		<xsl:template name="tr">
			<xsl:apply-templates select="./*" mode="column" />
		</xsl:template>
	</xsl:template>

	<xsl:template match="*" mode="row">
		<xsl:element name="tr">
			<xsl:apply-templates select="./*" mode="node" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="*" mode="column">
		<xsl:element name="th">
			<xsl:value-of select="translate(name(.),'qwertyuiopasdfghjklzxcvbnm_','QWERTYUIOPASDFGHJKLZXCVBNM ')" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="*" mode="node">
		<xsl:element name="td">
			<xsl:value-of select="." />
		</xsl:element>
	</xsl:template>

</xsl:stylesheet>