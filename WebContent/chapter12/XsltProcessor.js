//<![CDATA[
XsltProcessor.prototype = new XsltProcessor;
XsltProcessor.prototype.constructor = XsltProcessor;

/*
	Class:			XsltProcessor
	Function:		XsltProcessor
	Method:			n/a
	Description:	Constructor for this class.
*/
function XsltProcessor() {
  this._IE = (new RegExp('internet explorer','gi')).test(navigator.appName);
  this._xsl = new XMLDocument();		// Input XSL style sheet
  this._xml = new XMLDocument();		// Input XML document
  this._rinse = new XMLDocument();		// Input XML document
  this._output;                         // Output
  this._XMLSerializer;					// XML serializer (Gecko only)
  this._XSLTemplate;					// XSLT template (IE only)
  this._XsltProcessor;				    // XSLT processor

  if(!this._IE)
    this._XMLSerializer = new XMLSerializer();
}

/*
	Class:			XsltProcessor
	Function:		XsltProcessor_initialize
	Method:			_initialize
	Description:	Initializes/re-initializes the XSLT processor.
*/
function XsltProcessor_initialize() {
  if(this._IE) {
    this._XSLTemplate = new ActiveXObject('MSXML2.XSLTemplate.3.0');

    this._XSLTemplate.stylesheet = this._xsl.DOMDocument();

    this._XsltProcessor = this._XSLTemplate.createProcessor;
  } else
    this._XsltProcessor = new XSLTProcessor();
}
XsltProcessor.prototype._initialize = XsltProcessor_initialize;

/*
	Class:			XsltProcessor
	Function:		XsltProcessor_setParameter
	Method:			setParameter
	Description:	Inserts an XSLT parameter to the parameter cache.
*/
function XsltProcessor_setParameter(name,value) {
  try {
    if(this._IE)
      this._XsltProcessor.addParameter(name,value);
    else
      this._XsltProcessor.setParameter(null,name,value);
  }
  catch(e) {
    this._initialize();
    this.setParameter(name,value);
  }
}
XsltProcessor.prototype.setParameter = XsltProcessor_setParameter;

/*
	Class:			XsltProcessor
	Function:		XsltProcessor_load
	Method:			load
	Description:	Loads the XML document to be transformed.
*/
function XsltProcessor_load(xml) {
  try {
    this._xml.load(xml);
  }
  catch(e) {
    this._initialize();
  }
}
XsltProcessor.prototype.load = XsltProcessor_load;

/*
	Class:			XsltProcessor
	Function:		XsltProcessor_importStylesheet
	Method:			importStylesheet
	Description:	Loads the XSL style sheet for the transformation.
*/
function XsltProcessor_importStylesheet(xsl) {
  try {
    this._xsl.load(xsl);
  }
  catch(e) {
    this._initialize();
  }
}
XsltProcessor.prototype.importStylesheet = XsltProcessor_importStylesheet;

/*
	Class:			XsltProcessor
	Function:		XsltProcessor_readyState
	Method:			readyState
	Description:	Returns the readyState for a combination of the XML document and
					the XSL style sheet.
*/
function XsltProcessor_readyState() {
  switch(true) {
    case((this._xsl.readyState() == 0) && (this._xsl.readyState() == 0)):
      return(this._xsl.readyState());

      break;
    case((this._xsl.readyState() > 0) && (this._xsl.readyState() < 4)):
      return(this._xsl.readyState());

      break;
    case((this._xml.readyState() > 0) && (this._xml.readyState() < 4)):
      return(this._xml.readyState());

      break;
    default:
      return(4);

      break;
  }
}
XsltProcessor.prototype.readyState = XsltProcessor_readyState;

/*
	Class:       XsltProcessor
	Function:    XsltProcessor_transform
	Method:      transform
	Description: Performs the XSL transformation using the supplied XML document and XSL style sheet.
                Returns the result as an XML document.
*/
function XsltProcessor_transform() {
  if(this._IE) {
    this._XsltProcessor.input = this._xml.DOMDocument();

    this._XsltProcessor.transform();

    this._output = this._XsltProcessor.output;
  } else {
    this._XsltProcessor.importStylesheet(this._xsl.DOMDocument());

    this._output = this._XMLSerializer.serializeToString(this._XsltProcessor.transformToDocument(this._xml.DOMDocument(),document));
  }

  this._initialize();

  return(this._output);
}
XsltProcessor.prototype.transform = XsltProcessor_transform;

/*
	Class:			XsltProcessor
	Function:		XsltProcessor_serialize
	Method:			serialize
	Description:	Returns the result of the prior transformation as a serialize XML DOM
					document (text).
*/
function XsltProcessor_serialize() {
  return(this._output);
}
XsltProcessor.prototype.serialize = XsltProcessor_serialize;
// ]]> -->

