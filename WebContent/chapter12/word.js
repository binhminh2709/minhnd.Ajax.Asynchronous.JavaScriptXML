function word() {
  var _setCount = 0;          // Protected variable
  
  this.theWord;               // Public property
  this.setWord = _setWord;    // Public method setWord
  this.getWord = _getWord;    // Public method getWord
  this.count = _getSetCount;  // Public method count
  
  function _setWord(theWord) { // Public exposed as getWord
    this.theWord = theWord;
    _incrementCount();
  }
  function _getWord() {         // Public exposed as setWord
    return (this.theWord);
  }
  function _getSetCount() {     // Public exposed as count
    return (_setCount);
  }
  function _incrementCount() {  // Private method
    ++_setCount;
  }
}
