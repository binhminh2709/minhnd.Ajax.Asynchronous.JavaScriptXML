XMLDocument.prototype = new XMLDocument;
XMLDocument.prototype.constructor = XMLDocument;

/*
	Class:			XMLDocument
	Function:		XMLDocument
	Method:			n/a
	Description:	Constructor for this class.
*/
function XMLDocument() {
  this._IE = (new RegExp('internet explorer','gi')).test(navigator.appName);
  this._XMLHTTPRequest = new XMLHTTPRequest();
  this._XML;                            // XML DOM document
  this._DOMParser;                      // XML DOM parser (Gecko only)
  this._XMLSerializer;                  // XML serializer (Gecko only)
  this._state = 0;                      // Pseudo readyState

  if(!this._IE) {
    this._DOMParser = new DOMParser();
    this._XMLSerializer = new XMLSerializer();

    this._XML = document.implementation.createDocument("", "", null);
  }
}

/*
	Class:			XMLDocument
	Function:		XMLDocument_load
	Method:			load
	Description:	Loads the specified XML document.
*/
function XMLDocument_load(xml) {
  var isXMLText = false;
  var isXMLDocument = (typeof(xml) == 'object');

  try {									// Test for elements
    isXMLText = (new RegExp('<','g')).test(xml);
  }
  catch(e) { }

  switch (true) {
    case(this._IE && isXMLText):		// Internet Explorer & text XML Document
      this._XML = new ActiveXObject('MSXML2.FreeThreadedDOMDocument.3.0');
      
      this._XML.async = true;
      
      this._XML.loadXML(xml);
      this._state = 4;				    // Ready state is complete
      
      break;
    case(!this._IE && isXMLText):		// Not IE & text XML document
      this._XML = this._DOMParser.parseFromString(xml,"text/xml");
      this._state = 4;				    // Ready state is complete
      
      break;
    case(this._IE && isXMLDocument):	// Internet Explorer & XML DOM
      this._XML = new ActiveXObject('MSXML2.FreeThreadedDOMDocument.3.0');
    
      this._XML.async = true;
      
      try {
        this._XML.loadXML(xml.serialize());
      } catch(e) {
        this._XML = xml;
      }
      
      this._state = 4;				    // Ready state is complete
      
      break;
    case(!this._IE && isXMLDocument):	// Not IE & XML DOM
      try {
        this._XML = xml.DOMDocument();
      } catch(e) {
        this._XML = xml;
      }
      
      this._state = 4;				    // Ready state is complete
      
      break;
    default:
      this._XMLHTTPRequest.uri = xml;
      
      try {
        this._XMLHTTPRequest.send();
        
        this._state = 1;
      }
      catch(e) {
        if(this._IE) {
          this._XML = new ActiveXObject('MSXML2.FreeThreadedDOMDocument.3.0');
          
          this._XML.async = true;
        } else
          this._XML = this._DOMParser.parseFromString(' ','text/xml');

        this._state = 4;			    // Error - force complete
      }
  }

  if(this._state == 4)
    this._XMLHTTPRequest = new XMLHTTPRequest();
}
XMLDocument.prototype.load = XMLDocument_load;

/*
	Class:			XMLDocument
	Function:		XMLDocument_serialize
	Method:			serialize
	Description:	Returns the result of the prior transformation as a serialize XML DOM document (text).
*/
function XMLDocument_serialize() {
  try {
    if(this.readyState() == 4) {

      if(this._XMLHTTPRequest.readyState() == 4)
        this.load(this._XMLHTTPRequest.responseXML());

      if(this._IE)
        return(this._XML.xml)
      else
        return(this._XMLSerializer.serializeToString(this._XML));
    } else
			return(null);				 // Document not loaded
  }
  catch(e) {
    return(null);						 // Invalid document
  }
}
XMLDocument.prototype.serialize = XMLDocument_serialize;

/*
	Class:			XMLDocument
	Function:		XMLDocument_DOMDocument
	Method:			DOMDocument
	Description:	Returns the result of the prior transformation as an XML DOM document.
*/
function XMLDocument_DOMDocument() {
  try {
    if(this.readyState() == 4) {
      if(this._XMLHTTPRequest.readyState() == 4)
        this.load(this._XMLHTTPRequest.responseXML());

        return(this._XML);
    } else
      return(null);					    // Document not loaded
  }
  catch(e) {
    return(null);						// Invalid document
  }
}
XMLDocument.prototype.DOMDocument = XMLDocument_DOMDocument;

/*
	Class:			XMLDocument
	Function:		XMLDocument_readyState
	Method:			readyState
	Description:	Returns the readyState for the XML document.
*/
function XMLDocument_readyState() {
if(this._XMLHTTPRequest.readyState() == 0)
  return(4);
else
  return(this._XMLHTTPRequest.readyState());
}
XMLDocument.prototype.readyState = XMLDocument_readyState;

/*
	Class:			XMLDocument
	Function:		XMLHTTPRequest_setRequestHeader
	Method:			n/a
	Description:	Inserts to the cache of HTTP request headers.
*/
function XMLDocument_setRequestHeader(name,value) {
	this._XMLHTTPRequest.setRequestHeader(name,value);
}
XMLDocument.prototype.setRequestHeader = XMLDocument_setRequestHeader;

/*
	Class:			XMLDocument
	Function:		XMLDocument_getResponseHeader
	Method:			getResponseHeader
	Description:	Returns a single response header from the last XMLHTTP Request
					action.
*/
function XMLDocument_getResponseHeader(name) {
  return(this._XMLHTTPRequest.getResponseHeader(name));
}
XMLDocument.prototype.getResponseHeader = XMLDocument_getResponseHeader;

/*
	Class:			XMLDocument
	Function:		XMLDocument_getAllResponseHeaders
	Method:			getAllResponseHeaders
	Description:	Returns all of the response headers from the last XMLHTTP Request
					action.
*/
function XMLDocument_getAllResponseHeaders() {
  return(this._XMLHTTPRequest.getAllResponseHeaders());
}
XMLDocument.prototype.getAllResponseHeaders = XMLDocument_getAllResponseHeaders;

/*
	Class:			XMLDocument
	Function:		XMLDocument_setEnvelope
	Method:			setEnvelope
	Description:	Sets the envelope for an XMLHTTP Request.
*/
function XMLDocument_setEnvelope(value) {
  this._XMLHTTPRequest.envelope = value;
  this._XMLHTTPRequest.action = 'POST';
}
XMLDocument.prototype.setEnvelope = XMLDocument_setEnvelope;

/*
	Class:			XMLDocument
	Function:		XMLDocument_selectNodes
	Method:			selectNodes
	Description:	Returns an array of XML documents based upon an XPath
	                statement.
*/
function XMLDocument_selectNodes(xpath) {
  var results;
  var resultArray = new Array();            // XML Document result array

  if(this.readyState() == 4)
    if(this._XMLHTTPRequest.readyState() == 4)
      this.load(this._XMLHTTPRequest.responseXML());

  if(_IE) {
    results = this._XML.selectNodes(xpath);

    for(var i=0;i < results.length;i++) {
      resultArray.push(new XMLDocument());
      resultArray[i].load(results[i].xml);
    }
  } else {
    var evaluator = new XPathEvaluator();   // XPath evaluator
    var resolver = evaluator.createNSResolver(this._XML.documentElement);
    var result;                             // Single XPath result
    var xml;
    var i = 0;                              // Counter

    results = evaluator.evaluate(xpath,this._XML,resolver,XPathResult.ANY_TYPE,null);

    while(result = results.iterateNext()) {
      xml = document.implementation.createDocument("", "",null);

      xml.appendChild(xml.importNode(result,true));
      resultArray.push(new XMLDocument());
      resultArray[i].load(this._XMLSerializer.serializeToString(xml));

      ++i;
    }
  }

  return(resultArray);
}
XMLDocument.prototype.selectNodes = XMLDocument_selectNodes;

/*
	Class:			XMLDocument
	Function:		XMLDocument_selectSingleNode
	Method:			selectSingleNode
	Description:	Returns a single XML document based upon an XPath
	                statement.
*/
function XMLDocument_selectSingleNode(xpath) {
  return(this.selectNodes(xpath)[0]);
}
XMLDocument.prototype.selectSingleNode = XMLDocument_selectSingleNode;
