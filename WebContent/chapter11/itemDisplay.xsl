<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" indent="yes" media-type="text/html"/>

	<xsl:template match="/">
		<xsl:element name="div">
			<xsl:call-template name="row">
				<xsl:with-param name="string" select="'Guild Name:'" />
				<xsl:with-param name="top" select="'75px'" />
			</xsl:call-template>
			<xsl:call-template name="row">
				<xsl:with-param name="string" select="'Item Name:'" />
				<xsl:with-param name="top" select="'92px'" />
			</xsl:call-template>
			<xsl:call-template name="row">
				<xsl:with-param name="string" select="'Description:'" />
				<xsl:with-param name="top" select="'110px'" />
			</xsl:call-template>
			<xsl:call-template name="row">
				<xsl:with-param name="string" select="'Price:'" />
				<xsl:with-param name="top" select="'127px'" />
			</xsl:call-template>

			<xsl:call-template name="row">
				<xsl:with-param name="string" select="'guild_name'" />
				<xsl:with-param name="type" select="'data'" />
				<xsl:with-param name="top" select="'75px'" />
			</xsl:call-template>
			<xsl:call-template name="row">
				<xsl:with-param name="string" select="'item_name'" />
				<xsl:with-param name="type" select="'data'" />
				<xsl:with-param name="top" select="'92px'" />
			</xsl:call-template>
			<xsl:call-template name="row">
				<xsl:with-param name="string" select="'item_description'" />
				<xsl:with-param name="type" select="'data'" />
				<xsl:with-param name="top" select="'110px'" />
			</xsl:call-template>
			<xsl:call-template name="row">
				<xsl:with-param name="string" select="'item_price:'" />
				<xsl:with-param name="type" select="'data'" />
				<xsl:with-param name="top" select="'127px'" />
			</xsl:call-template>
		</xsl:element>
	</xsl:template>

	<xsl:template name="row">
		<xsl:param name="dataisland" select="' '" />
		<xsl:param name="string" />
		<xsl:param name="type" select="'header'" />
		<xsl:param name="top" />

		<xsl:variable name="apostrophe">'</xsl:variable>
		<xsl:variable name="nbsp">&amp;nbsp;</xsl:variable>

		<xsl:element name="div">
			<xsl:attribute name="class">rowHeader</xsl:attribute>
			<xsl:attribute name="style">
				<xsl:choose>
					<xsl:when test="$type = 'header'">
						<xsl:value-of select="concat($apostrophe,'position: absolute; left: 50px; right: auto%; bottom: auto; width: 200px; top: ',$top,$apostrophe)" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="concat($apostrophe,'position: absolute; left: 255px; right: auto%; bottom: auto; width: 600px; top: ',$top,$apostrophe)" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>

			<xsl:choose>
				<xsl:when test="$type = 'header'">
					<xsl:value-of disable-output-escaping="yes" select="concat($nbsp,$string)" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="xmlDI">
						<xsl:value-of select="$dataisland" />
					</xsl:attribute>
					<xsl:attribute name="xmlNode">
						<xsl:value-of select="$string" />
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:element>
	</xsl:template>

</xsl:stylesheet>